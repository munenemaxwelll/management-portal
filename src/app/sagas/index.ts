import {takeEvery } from "redux-saga/effects";
import { sagaActions } from "./sagaActions";


export function* fetchDataSaga() {
    
}
  

export function* rootSaga() {
    yield takeEvery(sagaActions.FETCH_DATA_SAGA, fetchDataSaga);
  }